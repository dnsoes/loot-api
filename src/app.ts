import http from 'http'; 
import { ExpressDriver } from './express/express';
require('dotenv').config()
const app = ExpressDriver.buildExpressDriver();
const server = http.createServer(app);

server.listen(process.env.PORT || 3000, ()=>{
    console.log('Success, server running on port:', process.env.PORT);
});