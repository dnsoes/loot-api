import express, { Router, NextFunction, Response, Request } from "express";
import bodyParser, { json } from "body-parser";
import cors from "cors";
import fs from "fs";
// import * as userHandlerFunctions from '../user-module/handler';

export class ExpressDriver {
  static app = express();

  static buildExpressDriver() {
    this.app.use(bodyParser.json());
    this.app.use(cors());
    this.app.use(ExpressRouteHandler.buildRouter());
    return this.app;
  }
}

export class ExpressRouteHandler {
  static router = Router();

  static buildRouter(): Router {
    this.router.get("/", (req: Request, res: Response, next: NextFunction) => {
      res.json({
        message: "Welcome to our Mobile app project backend! version 1.0.0"
      });
    });

    this.router.post("/login", (req: Request, res: Response, next: NextFunction) => {
      const username = req.body.username;
      const password = req.body.password;
      const users:any[] = fetchJsonFile('store.json');
      let userExists = false;
      for (const user of users) {
        if(user.username === username){
          userExists = true;
          if(user.password === password){
            res.json(
              {
                username: user.username,
                name: user.name,
                linkedAccounts: user.linkedAccounts,
              }
            )
          }else{
            res.status(400)
            res.json({Error: 'Invalid credentials'})
          }
        }
      }
      if(!userExists){
        res.status(400);
        res.json({Error: 'User not found'})
      }      
    })
    this.router.get(
      "/account_info/:username",
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const accountNumber: string = req.query.account_number as string
            ? req.query.account_number as string
            : "";
          const username = req.params.username;

          let jsonAccountArray: {
            username: string,
            password: string,
            name: string,
            linkedAccounts: {
                type: string,
                balance: string,
                currency: string,
                accountNumber: string,
                expDate: string,
                issuer: string,
            }[]
        }[]

          if (accountNumber.length<2) {
             jsonAccountArray = JSON.parse(fs.readFileSync("store.json", 'utf8'));

        // let fullFormattedUser: {
        //     username: string,
        //         password: string,
        //         name: string,
        //         linkedAccounts: {
        //             type: string,
        //             balance: string,
        //             currency: string,
        //             accountNumber: string,
        //             expDate: string,
        //             issuer: string,
        //             transactions: {
        //                 "type": string,
        //                 "amount": string,
        //                 "party": string,
        //                 "date": string,
        //                 "group": string
        //             }
        //         }[]
        // };

            let fullFormattedUser = await Promise.all(jsonAccountArray.map(async userAccount => { 
                if(userAccount.username === username){ 
                    const transactions = JSON.parse( await fs.readFileSync('transactions.json', 'utf8')); 
                    const allUserTransactions = transactions.map((transaction:any)=>{
                        return transaction[username];
                    })
                    userAccount.linkedAccounts = userAccount.linkedAccounts.map(account=>{
                        return {
                            ...account,
                            transactions: allUserTransactions[0][account.accountNumber]
                        }
                    })
                    return userAccount;
                }
                
            }))
            res.json(fullFormattedUser[0])
            console.log("array here", jsonAccountArray);
          } else {
             jsonAccountArray = JSON.parse(fs.readFileSync("store.json", 'utf8'))
            const fullFormattedUser = await Promise.all(jsonAccountArray.map(async (userAccount) => { 
                if(userAccount.username === username){
                    const requestedAccount = userAccount.linkedAccounts.map(account=>{
                        if(account.accountNumber === accountNumber){
                            return account
                        }
                    })

                    //repeated code
                    const transactions = JSON.parse( await fs.readFileSync('transactions.json', 'utf8')); 
                    const allUserTransactions = transactions.map((transaction:any)=>{
                        return transaction[username];
                    })
                    console.log(allUserTransactions)
                const fullFormattedUser = {
                    username: userAccount.username,
                    name: userAccount.name,
                    account: {
                    ...requestedAccount[0],
                    transactions: allUserTransactions[0][accountNumber]
                }}
                console.log(fullFormattedUser);

                return fullFormattedUser;
                 } } ) )
                 res.json(fullFormattedUser[0]);
                }
          res.json({});
        } catch (err) {
          res.json(err).status(400);
        }
      }
    );

    this.router.post(
      "/account_info/:username/link",
      (req: Request, res: Response, next: NextFunction) => {
        try {
          const acct_number: string = req.query.account_number as string;
          const username: string = req.params.username;
          const unlinked: any = JSON.parse(fs.readFileSync('unlinked-accounts.json','utf-8'));
          let transactions: any = JSON.parse(fs.readFileSync('transactions.json', "utf-8"));
          console.log(unlinked['2222-2222'])


          //Haven't finished linking accounts... 
          if(unlinked === null || unlinked[acct_number] === undefined){
            throw new Error('No unlinked accounts were found...')
          }else{ 
            console.log('hi from else')

            const fetchedAccount = unlinked[acct_number];
            
            const mappedTransactions = { 
              [acct_number] : fetchedAccount.transactions
            }
            for(let user of transactions){ 
              console.log('hi from for')

              if(user[username]){ 
                console.log('hi from if 1 ', unlinked[acct_number])
                
                user[username][acct_number] = fetchedAccount.transactions
                if(user[username][acct_number]){ 
                  console.log('hi from if 2')

                  console.log('Op Successful: Transactions successfully linked.')
                  //read accounts store.
                  const allAccounts: any = JSON.parse(fs.readFileSync('store.json','utf-8'));
                  for(let account of allAccounts){ 
                    if(account.username === username){ 
                      account.linkedAccounts.push(
                        {
                          "type": unlinked[acct_number].type,
                          "balance": unlinked[acct_number].balance,
                          "currency": unlinked[acct_number].currency,
                          "accountNumber": acct_number,
                          "expDate": unlinked[acct_number].expDate,
                          "issuer": unlinked[acct_number].issuer
                        }
                      )
                      delete unlinked[acct_number];
                      
                      console.log('last block')
                    }else{
                      throw new Error(`No Registered user user the username ${username} was found.`)
                    }
                  }
                  console.log(`All accounts`, allAccounts)
                  fs.writeFileSync('store.json', JSON.stringify(allAccounts), {encoding: 'utf-8', flag: 'w'}); 

                }else{
                  throw new Error('The account under ------ was not found')
                }
              }else{ 
                console.log('hi from else 2')

                throw new Error(`The specified user ${username} does not exist.`)
              }
            }
            fs.writeFileSync('transactions.json', JSON.stringify(transactions), {encoding: 'utf-8', flag: 'w'})
            fs.writeFileSync('unlinked-accounts.json', JSON.stringify(unlinked), {encoding: 'utf-8', flag: 'w'})

            res.send('')
          }
          
          console.log(unlinked);
        } catch (error) {
          res.json({error: error.message}).status(400);
        }

      }
    );

    this.router.delete("/account_info/:username/link",
    (req: Request, res: Response, next: NextFunction) => {
      try {
        console.log('here')
        const acctNum:string = req.query.account_number as string;
        const username: string = req.params.username as string; 

    
          let transactions: any = JSON.parse(fs.readFileSync('transactions.json', "utf-8"));
          let allAccounts: any = JSON.parse(fs.readFileSync('store.json','utf-8'));
          let unlinked: any = JSON.parse(fs.readFileSync('unlinked-accounts.json', 'utf-8'));
          for (const transaction of transactions) {
            if(transaction[username]){
              if(transaction[username][acctNum]){ 
                const foundTransactions = transaction[username][acctNum];
                console.log(foundTransactions)
                delete transaction[username][acctNum];

                for (const account of allAccounts) {
                  if(account.username === username){
                    let target:any = {};
                    let filteredArr = (account.linkedAccounts as []).filter((accountInfo: any)=>{
                      console.log(accountInfo.accountNumber !== acctNum)
                      if(accountInfo.accountNumber === acctNum){
                        target = accountInfo
                      }
                      return accountInfo.accountNumber !== acctNum;
                    })
                    account.linkedAccounts = filteredArr;
                    console.log('account to remove: ',target);
                    let unlinkedPayload = { 
                      ...target,
                      transactions: foundTransactions,
                    }
                    unlinked[target.accountNumber] = unlinkedPayload;
                    delete unlinked[target.accountNumber].accountNumber;

                    fs.writeFileSync('unlinked-accounts.json', JSON.stringify(unlinked), {encoding: 'utf-8', flag: 'w'}); 
                  }
                  else{
                    throw new Error(`No account under the username ${username} was found in our data store`)
                  }
                }

                fs.writeFileSync('store.json', JSON.stringify(allAccounts), {encoding: 'utf-8', flag: 'w'}); 
                
              }else{ 
                throw new Error(`No transactions were found with the specified account number ${acctNum}`)
              }
            }else{
              throw new Error(`No transactions were found with the specified username ${username}`);
            }
            
          }
          fs.writeFileSync('transactions.json', JSON.stringify(transactions), {encoding: 'utf-8', flag: 'w'}); 
          
      } catch (error) {
        res.json({error: error.message}).status(400);
      }
    }
    )
    this.router.post(
      "/account_info/:username/group",
      (req: Request, res: Response, next: NextFunction) => {
        try {
         const transactionQuery:{
           type: 'expense',
           party: string,
           accountNumber: string,
           group: string,
         }  = req.body.info

         const username = req.params.username;

         let transactions: any = JSON.parse(fs.readFileSync('transactions.json', "utf-8"));
         let userTransactions = [];
         for (const user of transactions) {

          if(user[username][transactionQuery.accountNumber]){
            console.log('found')
            userTransactions = user[username][transactionQuery.accountNumber];
          }else{
           console.log('No transactions found for the specified account Number')
          }
         }
         console.log(userTransactions)
         const updatedTransactions = userTransactions.map((transaction: any)=>{
           if(transaction.type=== transactionQuery.type &&
              transaction.party === transactionQuery.party){
                return { 
                  type: transaction.type,
                  amount: transaction.amount,
                  party: transaction.party,
                  date: '',
                  group: transactionQuery.group
                }
              }else{ 
                return transaction;
              }
         
         })

         for (const user of transactions) {
          if(user[username][transactionQuery.accountNumber]){
            user[username][transactionQuery.accountNumber] = updatedTransactions;
          }else{ 
            throw new Error('Transactions could not be updated.')
          }
        }

         fs.writeFileSync('transactions.json', JSON.stringify(transactions), {encoding: 'utf-8', flag: 'w'}); 

         res.json({Status: 'Operation complete', updatedTransactions})
        } catch (error) {
          res.json({error: error.message}).status(400);
        }
      }
    );

    this.router.delete(
      "/account_info/:username/group",
      (req: Request, res: Response, next: NextFunction) => {
        try {
           const transactionQuery:{
           type: 'expense',
           party: string,
           accountNumber: string,
           group: string,
         }  = req.body.info

         const username = req.params.username;

         let transactions: any = JSON.parse(fs.readFileSync('transactions.json', "utf-8"));
        } catch (error) {
          res.json({error: error.message}).status(400);

        }
      })

    return this.router;
  }

}

function fetchJsonFile(fileName:string){
  return JSON.parse(fs.readFileSync(fileName, 'utf-8'))
}
